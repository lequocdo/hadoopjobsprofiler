SE Group TU Dresden, Germany

---------How To Use------------------


Step 1: Convert logs to json format
 ./logs2json.py ~/crawler/hadoop/logs/ crawlerlogs

Step 2: Analyze the json files:
./jobprofile.py  -f crawlerlogs

The analyzing results will presented in files: jobsprofile.txt, tasksprofile.txt.