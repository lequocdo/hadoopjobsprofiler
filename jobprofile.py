#!/usr/bin/env python
######Hadoop job profile#########
######Author: Le Quoc Do#########
######SE Group TU Dresden########


import argparse
import json
import os
import sys
import glob



def parse_args(args):
  p = argparse.ArgumentParser(description='Extract job times')
  #g = p.add_mutually_exclusive_group(required=True)

  p.add_argument('-f','--input-logs',metavar='INPUT_LOGS'
                ,required=True
                ,help='folder containing the job events in json format')
  p.add_argument('-t','--time-unit',default='seconds',choices=['seconds']
                ,help='time unit to be displayed (for future releases)')

  return p.parse_args(args)

def getdiff(time_unit):
  if time_unit == 'seconds':
    f = lambda t1,t2:(float(t1)-float(t2)) / 1000
  else:
    f = lambda t1,t2:(float(t1)-float(t2))
  return f

def main():

  fjob = open("jobsprofile.txt","wb")
  ftask = open("tasksprofile.txt", "wb")
  args = parse_args(sys.argv[1:])
 
  os.chdir(args.input_logs)

  diff = getdiff(args.time_unit)
  print_id = False
  for input_file_name in glob.glob("*.json"):
    with open(input_file_name,"rt") as input_file:
      print(input_file_name)
      jobname = input_file_name.split(".")[0]
      print(jobname)
      #fjob.write(jobname + "|")
      jobid,job = json.load(input_file).items()[0]
       

    # Start Jobprofile.txt
    fjob.write(jobname + "|" +  str(job['submit_time']) + "|" + str(job['finish_time'])+ "|")
    fjob.write('{} {}'.format(
          jobid if print_id else ''
        , diff(job['finish_time'],job['submit_time'])))
    print job['counters']
    try:
      inload = job['counters']['org.apache.hadoop.mapred.FileInputFormat$Counter'][2]["BYTES_READ"][1]
      #print("Quoc" + inload)
      fjob.write("|" + str(inload))

      outload = job['counters']["org.apache.hadoop.mapred.FileOutputFormat$Counter"][2]["BYTES_WRITTEN"][1]
      fjob.write("|"+ str(outload) + "\n") #End jobsprofile.txt
    except:
      inload = job['counters']['org.apache.hadoop.mapreduce.lib.input.FileInputFormat$Counter'][2]["BYTES_READ"][1]
      #print("Quoc" + inload)
      fjob.write("|" + inload)

      outload = job['counters']["org.apache.hadoop.mapreduce.lib.output.FileOutputFormat$Counter"][2]["BYTES_WRITTEN"][1]
      fjob.write("|"+ outload + "\n") #End jobsprofile.txt


    #Start Tasksprofile.txt
    #ftask.write(jobname + "|")
    tasks = job['maps'].items()
    #ftask.write("Map|")
    for taskid, task in tasks:
      ftask.write(jobname + "|")
      ftask.write("Map|")
      ftask.write(taskid + "|")
      start = task["start_time"]
      finish = task["finish_time"]
      ftask.write(str(start) + "|")
      ftask.write(str(finish) + "|")
      ftask.write('{} {}'.format(jobid if print_id else '', diff(finish,start)) + "|")
      intask =task["counters"]["FileSystemCounters"][2]["HDFS_BYTES_READ"][1]
      ftask.write(intask + "|")
      outtask = task["counters"]["FileSystemCounters"][2]["FILE_BYTES_WRITTEN"][1]
      ftask.write(outtask + "\n\t\t")


    tasks = job['reduces'].items()


    #Shuffle
    for taskid, task in tasks:
      ftask.write(jobname + "|")
      ftask.write("Shuffle|")
      ftask.write(taskid + "|")
      start = task["start_time"]
      attempts = task["successful_attempt"].items()
      for attemptid, attempt in attempts:
          finish = attempt["shuffle_finished"]
      ftask.write(str(start) + "|")
      ftask.write(str(finish) + "|")
      ftask.write('{} {}'.format(jobid if print_id else '', diff(finish,start)) + "|")

      intask =task["counters"]["org.apache.hadoop.mapred.Task$Counter"][2]["REDUCE_SHUFFLE_BYTES"][1]
      ftask.write(intask + "|")
      outtask = task["counters"]["org.apache.hadoop.mapred.Task$Counter"][2]["REDUCE_SHUFFLE_BYTES"][1]
      ftask.write(outtask + "\n\t\t")

    #Sort
    for taskid, task in tasks:
      ftask.write(jobname + "|")
      ftask.write("Sort|")
      ftask.write(taskid + "|")
      #start = finish
      #start = task["shuffle_finished"]

      attempts = task["successful_attempt"].items()
      for attemptid, attempt in attempts:
          start = attempt["shuffle_finished"]
          finish = attempt["sort_finished"]
      ftask.write(str(start) + "|")
      ftask.write(str(finish) + "|")
      ftask.write('{} {}'.format(jobid if print_id else '', diff(finish,start)) + "|")

      intask = "0"
      ftask.write(intask + "|")
      outtask = "0"
      ftask.write(outtask + "\n\t\t")


    #Reduce
    for taskid, task in tasks:
      ftask.write(jobname + "|")
      ftask.write("Reduce|")
      ftask.write(taskid + "|")

      attempts = task["successful_attempt"].items()
      for attemptid, attempt in attempts:
          start = attempt["sort_finished"]

      finish = task["finish_time"]
      ftask.write(str(start) + "|")
      ftask.write(str(finish) + "|")
      ftask.write('{} {}'.format(jobid if print_id else '', diff(finish,start)) + "|")

      intask = "-" 
      ftask.write(intask + "|")
      outtask = task["counters"]["FileSystemCounters"][2]["FILE_BYTES_WRITTEN"][1]
      ftask.write(outtask + "\n\t\t")



    #Full Reduce
    #ftask.write("Reduce|")
    for taskid, task in tasks:
      ftask.write(jobname + "|")
      ftask.write("Full Reduce|")
      ftask.write(taskid + "|")
      start = task["start_time"]
      finish = task["finish_time"]
      ftask.write(str(start) + "|")
      ftask.write(str(finish) + "|")
      ftask.write('{} {}'.format(jobid if print_id else '', diff(finish,start)) + "|")

      intask =task["counters"]["FileSystemCounters"][2]["FILE_BYTES_READ"][1]
      ftask.write(intask + "|")
      outtask = task["counters"]["FileSystemCounters"][2]["FILE_BYTES_WRITTEN"][1]
      ftask.write(outtask + "\n\t\t")

    
    ftask.write("\n")
    #End Taskprofile.txt

if __name__=='__main__':
  main()
